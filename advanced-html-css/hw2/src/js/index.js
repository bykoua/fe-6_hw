const burgerMenu = document.querySelector(".burger__menu");
const headerMenu = document.querySelector(".menu");

burgerMenu.addEventListener("click", () => {
    burgerMenu.classList.toggle("open");
    headerMenu.classList.toggle("open");
})