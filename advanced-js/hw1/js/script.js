"use strict";

class Employee {
    constructor(props) {
        this._age = props.age;
        this._salary = props.salary;
        this._name = props.name;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(props) {
        super(props);
        this._lang = props.lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }
}

const worker1 = new Employee({
    name: "Petro",
    age: 33,
    salary: 14500
});

const worker2 = new Employee({
    name: "Ivan",
    age: 25,
    salary: 23900
});

const programmer1 = new Programmer({
    name: "Dmytro",
    age: 21,
    salary: 15000,
    lang: ["JS", "React", "Vue.js"]
});

const programmer2 = new Programmer({
    name: "Andriy",
    age: 34,
    salary: 29000,
    lang: ["C#", ".Net", "Java"]
});

console.log(worker1);
console.log(worker2);
console.log(programmer1);
console.log(programmer2);