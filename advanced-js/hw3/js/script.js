"use strict";
// TASK 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clients = [...new Set([...clients1, ...clients2])];

console.group("TASK 1");
console.log(`Unique client's list: ${clients}.`);
console.groupEnd();

// TASK 2

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = ({gender, status, ...rest}) => rest;
console.group("TASK 2");
characters.forEach(e => {
    console.group(`${e.name} short info`);
    console.log(charactersShortInfo(e));
    console.groupEnd();
});
console.groupEnd();

// TASK 3

const user1 = {
    name: "John",
    years: 30
};

const {name, years: age, isAdmin = false} = user1;

console.group("TASK 3");
console.log(`Name: ${name}`);
console.log(`Age: ${age}`);
console.log(`Admin status: ${isAdmin}`);
console.groupEnd();

// TASK 4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
};

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};

const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};

console.group("TASK 4");
console.log(fullProfile);
console.groupEnd();

// TASK 5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};

console.group("TASK 5");
console.log([...books,bookToAdd]);
console.groupEnd();


// TASK 6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const newEmployee = { ...employee, age: 33, salary: 20000 };
console.log("TASK 6");
console.log(newEmployee);
console.groupEnd();

// TASK 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array

alert("TASK 7");
alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'

