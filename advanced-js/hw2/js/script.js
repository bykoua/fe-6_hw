"use strict";

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const bookPrinter = (arr) => {
    const root = document.querySelector("#root")
    const ul = document.createElement("ul");

    let props = [];
    arr.forEach((el) => {
        props.push(...Reflect.ownKeys(el));
    });

    const uniqProps = new Set(props);

    arr.forEach((el, index) => {
        try {
            uniqProps.forEach(prop => {
                if (!el.hasOwnProperty(prop)) {
                    throw new Error(`Index: ${index + 1}. No ${prop}!`);
                }
            })

            const {author, name, price} = el;

            const li = document.createElement("li");
            const h2 = document.createElement("h2");
            const h3 = document.createElement("h3");
            const p = document.createElement("p");

            h2.textContent = `Book name: "${name}"`;
            h3.textContent = `Author: ${author}`;
            p.textContent = `Price: ${price}`;

            li.append(h2, h3, p);
            ul.append(li);
        } catch (e) {
            console.error(e.message);
        }
    });
    root.append(ul);
};

bookPrinter(books);